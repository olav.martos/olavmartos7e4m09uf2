﻿using System;
using System.Threading;

namespace P2Sopar_Filosofos
{
    class Filosofo
    {
        // Atributos
        public int id;
        public Palillo palilloIzquierdo;
        public Palillo palilloDerecho;
        public bool comido = false;

        // Constructor
        public Filosofo(int id, Palillo izquierdo, Palillo derecho)
        {
            this.id = id;
            palilloIzquierdo = izquierdo;
            palilloDerecho = derecho;
        }

        // Metodos

        /// <summary>
        /// Metodo que simula el proceso de pensar de cada filosofo, donde se queda pensando en un tema durante un tiempo
        /// </summary>
        public void Pensar()
        {
            // Pensamientos filosoficos
            Random rnd = new Random();
            string[] frases = new string[] { "Pienso, luego existo", "Dios ha muerto", "Solo sé que no sé nada", "El conocimiento es poder", "El corazón tiene razones que la razón ignora" };

            // Mensaje y tiempo de espera
            Console.WriteLine($"{this} esta pensando sobre: \"{frases[rnd.Next(0, frases.Length)]}\".");
            Thread.Sleep(1000);
        }

        /// <summary>
        /// Metodo que simula el proceso de comer de cada filosofo. 
        /// Pilla primero un palillo, luego el otro, come durante un tiempo y los suelta
        /// </summary>
        public void Comer()
        {
            Console.WriteLine($"\n{this} esta intentando tomar el {palilloIzquierdo} y el {palilloDerecho}, esperando...");
            lock (palilloIzquierdo)
            {
                Console.WriteLine($"{this} ha tomado el {palilloIzquierdo}.");
                lock (palilloDerecho)
                {
                    Console.WriteLine($"{this} ha tomado el {palilloDerecho}.");
                    Console.WriteLine($"{this} esta comiendo...");
                    Thread.Sleep(2000);
                }
                Console.WriteLine($"{this} ha soltado el {palilloDerecho}.");
            }
            Console.WriteLine($"{this} ha soltado el {palilloIzquierdo}.\n");
            comido = !comido;
        }


        /// <summary>
        /// Metodo que hace que el filosofo piense y luego coma
        /// </summary>
        public void Ejecutar()
        {
            while (!comido)
            {
                Pensar();
                Comer();
            }
        }

        /// <summary>
        /// Metodo override que sirve para decir de que filosofo estamos hablando
        /// </summary>
        /// <returns>Devuelve la frase "El filosofo nº {id_del_filosofo}"</returns>
        public override string ToString() { return $"El filosofo nº {id}"; }
    }
}
