﻿using System;
using System.Threading;

namespace ThreadNevera
{
    class ReunionPersonas
    {
        static void Main(string[] args)
        {
            // Instanciamos la nevera y un array de hilos.
            Nevera nevera = new Nevera();
            Thread[] hilos = new Thread[]
            {
                new Thread(() => { nevera.OmplirNevera("Anitta"); }),
                new Thread(() => { nevera.BeureCervesa("Bad Bunny"); }),
                new Thread(() => { nevera.BeureCervesa("Lil Nas X"); }),
                new Thread(() => { nevera.BeureCervesa("Manuel Turizo"); })
            };

            // Iniciamos los hilos y hacemos su respectivo Join()
            foreach(Thread hilo in hilos)
            {
                hilo.Start();
                hilo.Join();
            }

            // Mostramos la cantidad de cervezas que quedan
            Console.WriteLine($"Quedan {nevera.numCervezas()} cervezas en la nevera");

            // Termina la ejecucion
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nPulsa una tecla per terminar");
            Console.ResetColor();

        }
    }
}
