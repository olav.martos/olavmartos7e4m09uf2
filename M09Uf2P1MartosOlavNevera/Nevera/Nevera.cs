﻿using System;
using System.Collections.Generic;

namespace ThreadNevera
{
    class Nevera
    {
        // Atributos
        private const int max = 9;
        public List<int> cervezas { get; set; }
        private Random rnd { get; set; }

        // Constructor
        public Nevera()
        {
            cervezas = new List<int>();
            rnd = new Random();
        }

        // Metodos
        /// <summary>
        /// Metodo que nos permite añadir un numero aleatorio de cervezas a la nevera
        /// </summary>
        /// <param name="persona">Persona que realiza la accion</param>
        public void OmplirNevera(string persona)
        {
            int numero = rnd.Next(1, 6);
            if (cervezas.Count + numero > max) { numero = max - cervezas.Count; }
            for (int i = 0; i < numero; i++) { cervezas.Add(1);  }


            Console.WriteLine($"{persona} ha llenado la nevera con {numero} cervezas");
        }

        /// <summary>
        /// Metodo que nos permite quitar un numero aleatorio de cervezas a la nevera
        /// </summary>
        /// <param name="persona">Persona que realiza la acción</param>
        public void BeureCervesa(string persona)
        {
            if (cervezas.Count == 0)
            {
                Console.WriteLine($"Lo siento, {persona} pero la nevera esta vacia");
                return; 
            }

            int numero = rnd.Next(1, 6);

            if(numero > cervezas.Count) { numero = cervezas.Count; }
            for (int i = 0; i < numero; i++) { cervezas.RemoveAt(0); }

            Console.WriteLine($"{persona} ha bebido {numero} cervezas");
        }

        /// <summary>
        /// Metodo adicional para no tener que hacer nevera.cervezas.Count
        /// </summary>
        /// <returns>La cantidad de cervezas actuales</returns>
        public int numCervezas() { return cervezas.Count; }
    }
}
