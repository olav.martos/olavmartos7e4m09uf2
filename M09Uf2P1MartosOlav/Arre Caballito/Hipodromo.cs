﻿using System;

namespace Arre_Caballito
{
    class Hipodromo
    {
        static void Main(string[] args)
        {
            // Cambiamos el tamaño de la consola
            Console.SetWindowSize(200, Console.WindowHeight);

            // Creamos una carrera
            Carrera carrera = new Carrera();

            // Pedimos que presione una tecla determinada para iniciar la carrera
            int count = 0;
            PressKey(ConsoleColor.Cyan);
            while (Console.ReadKey().Key != ConsoleKey.C)
            {
                count++;
                if (count >= 50) { PressKey(ConsoleColor.Red); }
                else if (count >= 25) { PressKey(ConsoleColor.Green); }
                else { PressKey(ConsoleColor.Cyan); }
            }
            carrera.IniciarCarrera();

            // Termina la ejecucion
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;

        }

        static void PressKey(ConsoleColor color)
        {
            // Mostramos la tecla de un color diferente dependiendo de los intentos
            Console.Clear();
            Console.Write("Presiona la tecla ");

            Console.ForegroundColor = color;
            Console.Write("[C]");
            Console.ResetColor();

            Console.WriteLine(" para iniciar la carrera");
        }
    }
}
