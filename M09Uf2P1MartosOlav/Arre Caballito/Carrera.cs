﻿using System;
using System.Threading;

namespace Arre_Caballito
{
    class Carrera
    {
        // Atributos
        public Caballo[] caballos;
        private Random rnd = new Random();
        private int minV = 3;
        private int maxV = 7;

        // Constructor
        public Carrera()
        {
            string[] nombres = { "Juan", "Emilio", "Cuesta", "Castor", "Redes", "El Prohibido" };
            caballos = new Caballo[] {
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV)),
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV)),
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV)),
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV)),
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV)),
                new Caballo(nombres[rnd.Next(0,nombres.Length)], rnd.Next(minV, maxV))
            };
        }

        // Metodos

        /// <summary>
        /// Metodo que inicia una carrera
        /// </summary>
        public void IniciarCarrera()
        {
            // Iniciamos los hilos
            for (int i = 0; i < caballos.Length; i++) caballos[i].hilo.Start();

            // Carrera
            while (true)
            {
                Console.Clear();
                Console.Write("Posiciones:");
                for (int i = 0; i < caballos.Length; i++)
                {
                    ComprobarNombre(caballos[i], i);
                    for (int j = 0; j < caballos[i].posicion; j++) { Console.Write("-"); }
                    caballos[i].velocidad = rnd.Next(minV, maxV);
                    if (Winner(caballos, caballos[i], i)) { return; }
                }
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Comprobamos los nombres de los caballos para poner un tabulador o dos tabuladores
        /// </summary>
        /// <param name="caballo">Caballo que estamos escribiendo</param>
        /// <param name="i">Numero del caballo que esta escribiendo</param>
        private void ComprobarNombre(Caballo caballo, int i)
        {
            i++;
            if (caballo.nombre == "El Prohibido") { Console.Write($"\nCaballo {i} - {caballo.nombre}: \t"); }
            else { Console.Write($"\nCaballo {i} - {caballo.nombre}: \t\t"); }
        }

        /// <summary>
        /// Metodo que determina si un caballo ha ganado al llegar a cien
        /// </summary>
        /// <param name="caballo">Caballo que queremos averiguar si ha ganado</param>
        /// <param name="numCaballo">Numero de caballo que queremos saber si ha gando</param>
        /// <returns>Devuelve true si un caballo ha ganado. False si no.</returns>
        private bool Winner(Caballo[] caballos, Caballo caballo, int numCaballo)
        {
            if (caballo.posicion >= 100)
            {
                // Indicamos al caballo ganador.
                caballo.puesto = 1;

                // Mostramos las posiciones
                Console.Clear();
                Console.Write("Posiciones finales:");
                MostrarPosiciones(caballos);
                if (caballo.puesto == 1) { Console.ForegroundColor = ConsoleColor.Yellow; }
                Console.WriteLine($"\n\nEl ganador es el Caballo {numCaballo + 1} - {caballo.nombre}");
                Console.ResetColor();

                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Muestra las posiciones de cada caballo al final de la carrera
        /// </summary>
        /// <param name="caballos">Array de caballos</param>
        private void MostrarPosiciones(Caballo[] caballos)
        {
            int numCaballo = 0;
            foreach (Caballo caballo in caballos)
            {
                if (caballo.puesto == 1) { Console.ForegroundColor = ConsoleColor.Yellow; }
                ComprobarNombre(caballo, numCaballo);
                for (int i = 0; i < caballo.posicion; i++) { Console.Write("-"); }
                numCaballo++;
                Console.ResetColor();
            }
        }

    }
}
