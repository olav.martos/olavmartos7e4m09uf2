﻿using System;
using System.Threading;

namespace P2Sopar_Filosofos
{
    class SoparFilosofos
    {
        static void Main(string[] args)
        {
            // Creamos los cinco palillos y los guardamos en un array
            Palillo[] palillos = new Palillo[]
            {
                new Palillo(0),
                new Palillo(1),
                new Palillo(2),
                new Palillo(3),
                new Palillo(4)
            };

            // Creamos a los cinco filosofos y los guardamos en un array
            Filosofo[] filosofos = new Filosofo[]
            {
                new Filosofo(1, palillos[0], palillos[1]),
                new Filosofo(2, palillos[1], palillos[2]),
                new Filosofo(3, palillos[2], palillos[3]),
                new Filosofo(4, palillos[3], palillos[4]),
                new Filosofo(5, palillos[4], palillos[0])
            };


            // Creamos un array de hilos que van ejecutar el metodo Ejecutar de cada filosofo e iniciamos dichos hilos
            Thread[] hilos = new Thread[5];
            for (int i = 0; i < filosofos.Length; i++)
            {
                hilos[i] = new Thread(new ThreadStart(filosofos[i].Ejecutar));
                hilos[i].Start();
            }

            // Hacemos un Join() de esos hilos
            foreach (Thread hilo in hilos) { hilo.Join(); }

            // Termina la ejecucion
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nPulsa una tecla per terminar");
            Console.ResetColor();

        }
    }
}
