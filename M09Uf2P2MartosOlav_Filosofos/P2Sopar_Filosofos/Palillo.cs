﻿namespace P2Sopar_Filosofos
{
    class Palillo
    {
        // Atributos
        public int id { get; set; }

        // Constructor
        public Palillo(int id)
        {
            this.id = id;
        }

        /// <summary>
        /// Metodo override utilizado para saber de que palillo estamos hablando
        /// </summary>
        /// <returns>Devuelve la frase "palillo nº{id_palillo}</returns>
        public override string ToString() { return $"palillo nº{id}"; }
    }
}
