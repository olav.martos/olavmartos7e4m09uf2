﻿using System.Threading;

namespace Arre_Caballito
{
    class Caballo
    {
        // Atributos
        public string nombre;
        public int posicion;
        public int velocidad;
        public Thread hilo;
        public int puesto;

        // Constructor
        public Caballo(string nombre, int velocidad)
        {
            this.nombre = nombre;
            posicion = 0;
            this.velocidad = velocidad;
            hilo = new Thread(Avanzar);
            puesto = 0;
        }

        // Metodos

        /// <summary>
        /// Metodo que permite que el caballo pueda avanzar sumando su velocidad
        /// </summary>
        public void Avanzar()
        {
            while (posicion < 100)
            {
                posicion += velocidad;
                Thread.Sleep(1000);
            }
        }
    }

}
